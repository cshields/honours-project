	#version 440

	layout (binding = 1, rgba32f) uniform imageBuffer vertices;
	layout (binding = 2, rgba32f) uniform imageBuffer normals;

	layout (location = 0) uniform mat4 modelMatrix;
	layout (location = 1) uniform mat3 normalMatrix;

	void main(void)
	{
		int index = gl_VertexID;
		vec4 newPos = modelMatrix * imageLoad(vertices, index);
		imageStore(vertices, index, newPos);

		//imageStore(normals, index, vec4(normalMatrix * imageLoad(normals, index).xyz, 1.0f));
	}