#include "scene.h"


scene::scene(GLuint width, GLuint height)
{
	screenWidth = width;
	screenHeight = height;
}


scene::~scene()
{
}

void scene::init(){
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	// complie shader programs
	shader = gl::createComputeShader("ray tracer.comp");
	quadProgram = gl::initShader("quad.vert", "quad.frag");

	glUseProgram(shader);
	cam = new camera(shader, 60.0f, glm::vec2(screenWidth, screenHeight));

	// generate the screen texture  
	GLuint imgTex;
	glGenTextures(1, &imgTex);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, imgTex);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, screenWidth, screenHeight);
	glBindImageTexture(0, imgTex, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA8);

	render = new renderer(shader, quadProgram, screenWidth, screenHeight);

	// hides and centres mouse pointer
	if (SDL_SetRelativeMouseMode(SDL_TRUE) != 0)
		exit(0);
}

bool scene::update(){
	const Uint8* keys = SDL_GetKeyboardState(NULL); // retreive keyboard key states

	// create a frame time delta to scale movement speed
	ending = std::chrono::steady_clock::now();
	float delta = std::chrono::duration_cast<std::chrono::microseconds>(ending - start).count() / million;
	start = std::chrono::steady_clock::now();

	if (keys[SDL_SCANCODE_W]) cam->moveForward(6.0f*delta);
	if (keys[SDL_SCANCODE_S]) cam->moveForward(-6.0f*delta);
	if (keys[SDL_SCANCODE_A]) cam->moveRight(6.0f*delta);
	if (keys[SDL_SCANCODE_D]) cam->moveRight(-6.0f*delta);
	if (keys[SDL_SCANCODE_R]) cam->moveY(6.0f*delta);
	if (keys[SDL_SCANCODE_F]) cam->moveY(-6.0f*delta);

	if (keys[SDL_SCANCODE_UP]) render->setLightPos(glm::vec4(0.0f, 0.0f, -6.0f*delta, 0.0f));
	if (keys[SDL_SCANCODE_DOWN]) render->setLightPos(glm::vec4(0.0f, 0.0f, 6.0f*delta, 0.0f));
	if (keys[SDL_SCANCODE_LEFT]) render->setLightPos(glm::vec4(-6.0f*delta, 0.0f, 0.0f, 0.0f));
	if (keys[SDL_SCANCODE_RIGHT]) render->setLightPos(glm::vec4(6.0f*delta, 0.0f, 0.0f, 0.0f));

	cam->rotateCamera();

	if (keys[SDL_SCANCODE_ESCAPE]) return false;
	return true;
}

void scene::draw(SDL_Window *window){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	render->sceneDraw(cam->getView(), cam->getEye());

	SDL_GL_SwapWindow(window); // swap buffers
}

