#define million 1000000.0f

#include <stack>
#include "gl.h"
#include "renderer.h"
#include "camera.h"
#include <chrono>

using namespace std;

class scene
{
private:

	GLuint shader, quadProgram;

	GLuint screenHeight;
	GLuint screenWidth;

	std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now(),
	ending = std::chrono::steady_clock::now();

	renderer *render;
	camera* cam;
public:
	scene(GLuint width, GLuint height);
	~scene();
	void init();
	bool update();
	void draw(SDL_Window *window);
};
