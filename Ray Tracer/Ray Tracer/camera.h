#pragma once
#include "gl.h"
class camera
{
private:
	glm::vec3 eye, at, up, facing, eyeDir;
	GLfloat rX, rY;

	void Orientate(glm::vec2 xy);

public:
	camera(GLuint shader, GLfloat vert, glm::vec2 screenRes);
	~camera();

	glm::mat4 getView(){ return glm::lookAt(eye, at, up); }
	glm::vec3 getEye(){ return eye; }
	void rotateCamera();
	void moveForward(GLfloat d);
	void moveRight(GLfloat d);
	void moveY(GLfloat d);
};

