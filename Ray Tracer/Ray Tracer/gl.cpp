#include "gl.h"

using namespace std;

namespace gl
{
	void exitFatalError(const char *message) {
		cout << message << " ";
		exit(1);
	}

	// loadFile - loads text file from file fname as a char* 
	// Allocates memory - so remember to delete after use
	// size of file returned in fSize
	char* loadFile(const char *fname, GLint &fSize) {
		int size;
		char * memblock;

		// file read based on example in cplusplus.com tutorial
		// ios::ate opens file at the end
		ifstream file(fname, ios::in | ios::binary | ios::ate);
		if (file.is_open()) {
			size = (int)file.tellg(); // get location of file pointer i.e. file size
			fSize = (GLint)size;
			memblock = new char[size];
			file.seekg(0, ios::beg);
			file.read(memblock, size);
			file.close();
			cout << "file " << fname << " loaded" << endl;
		}
		else {
			cout << "Unable to open file " << fname << endl;
			fSize = 0;
			// should ideally set a flag or use exception handling
			// so that calling function can decide what to do now
			return nullptr;
		}
		return memblock;
	}

	GLuint initShader(const char *vertFile, const char *fragFile){

		GLuint vertShader, fragShader, program;
		vertShader = glCreateShader(GL_VERTEX_SHADER);
		fragShader = glCreateShader(GL_FRAGMENT_SHADER);

		if (0 == vertShader)
		{
			fprintf(stderr, "Error creating vertex shader.\n");
			exit(1);
		}

		if (0 == fragShader)
		{
			fprintf(stderr, "Error creating frag shader.\n");
			exit(1);
		}


		GLint vLen, fLen;
		const GLchar *vertCode, *fragCode, *compCode;
		vertCode = loadFile(vertFile, vLen);
		fragCode = loadFile(fragFile, fLen);
		glShaderSource(vertShader, 1, &vertCode, &vLen);
		glShaderSource(fragShader, 1, &fragCode, &fLen);

		glCompileShader(vertShader);
		GLint result;
		glGetShaderiv(vertShader, GL_COMPILE_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Vertex shader compilation failed.\n");
			GLint logLen;
			glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(vertShader, logLen, &written, log);
				fprintf(stderr, "Shader log:\n%s", log);
				free(log);
			}
		}

		glCompileShader(fragShader);
		glGetShaderiv(fragShader, GL_COMPILE_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Frag shader compilation failed.\n");
			GLint logLen;
			glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(fragShader, logLen, &written, log);
				fprintf(stderr, "Shader log:\n%s", log);
				free(log);
			}
		}

		program = glCreateProgram();
		if (0 == program)
		{
			fprintf(stderr, "Error creating program object.\n");
			exit(1);
		}

		glAttachShader(program, vertShader);
		glAttachShader(program, fragShader);

		glLinkProgram(program);
		glGetProgramiv(program, GL_LINK_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Failed to link shader program!\n");
			GLint logLen;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(program, logLen, &written, log);
				fprintf(stderr, "Program log:\n%s", log);
				free(log);
			}
			else
				glUseProgram(program);
		}

		delete[] vertCode;
		delete[] fragCode;

		return program;
	}

	GLuint createComputeShader(const char *compFile){
		GLuint compShader, program;
		compShader = glCreateShader(GL_COMPUTE_SHADER);

		if (0 == compShader)
		{
			fprintf(stderr, "Error creating comp shader.\n");
			exit(1);
		}

		const GLchar *compCode;
		GLint cLen;
		compCode = loadFile(compFile, cLen);
		glShaderSource(compShader, 1, &compCode, &cLen);

		glCompileShader(compShader);
		GLint result;
		glGetShaderiv(compShader, GL_COMPILE_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Comp shader compilation failed.\n");
			GLint logLen;
			glGetShaderiv(compShader, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(compShader, logLen, &written, log);
				fprintf(stderr, "Shader log:\n%s", log);
				free(log);
			}
		}

		program = glCreateProgram();
		if (0 == program)
		{
			fprintf(stderr, "Error creating program object.\n");
			exit(1);
		}

		glAttachShader(program, compShader);

		glLinkProgram(program);
		glGetProgramiv(program, GL_LINK_STATUS, &result);
		if (GL_FALSE == result)
		{
			fprintf(stderr, "Failed to link shader program!\n");
			GLint logLen;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);
			if (logLen > 0)
			{
				char *log = (char *)malloc(logLen);
				GLsizei written;
				glGetShaderInfoLog(program, logLen, &written, log);
				fprintf(stderr, "Program log:\n%s", log);
				free(log);
			}
			else
				glUseProgram(program);
		}
		delete[] compCode;

		return program;
	}

	void setUniform(const GLuint program, const int uniformIndex, const glm::mat4 data) {
		glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, &data[0][0]);
	}

	void setUniform(const GLuint program, const int uniformIndex, const glm::mat3 data) {
		glUniformMatrix3fv(uniformIndex, 1, GL_FALSE, &data[0][0]);
	}

	void setUniform(const GLuint program, const int uniformIndex, const glm::vec4 data) {
		glUniform4fv(uniformIndex, 1, &data[0]);
	}

	void setUniform(const GLuint program, const int uniformIndex, const glm::vec3 data) {
		glUniform3fv(uniformIndex, 1, &data[0]);
	}

	void setUniform(const GLuint program, int uniformIndex, const glm::vec2 data){
		glUniform2fv(uniformIndex, 1, &data[0]);
	}

	void setUniform(const GLuint program, const int uniformIndex, const GLfloat data) {
		glUniform1fv(uniformIndex, 1, &data);
	}

	void setUniform(const GLuint program, const int uniformIndex, const int data) {
		glUniform1iv(uniformIndex, 1, &data);
	}

	GLuint importMesh(const aiScene* mesh){

		GLuint VAO;
		// generate and set up a VAO for the mesh
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);

		GLuint VBO;
		glGenBuffers(1, &VBO);

		vector<glm::vec3> v;
		vector<glm::vec2> t;
		vector<glm::vec3> n;
		vector<unsigned int> indices;
		vector<glm::vec3> tan;
		vector<glm::vec3> bitan;

		for (int i = 0; i < mesh->mMeshes[0]->mNumVertices; i++){
			aiVector3D* verts = &mesh->mMeshes[0]->mVertices[i];
			v.push_back(glm::vec3(verts->x, verts->y, verts->z)); // vertex buffer

			if (mesh->mMeshes[0]->HasNormals() == true){
				aiVector3D* norms = &mesh->mMeshes[0]->mNormals[i];
				n.push_back(glm::vec3(norms->x, norms->y, norms->z)); // normal buffer
			}

			if (mesh->mMeshes[0]->HasTextureCoords(0) == true){
				aiVector3D* tex_coords = &mesh->mMeshes[0]->mTextureCoords[0][i]; // UV buffer
				t.push_back(glm::vec2(tex_coords->x, tex_coords->y));
			}

			if (mesh->mMeshes[0]->HasTangentsAndBitangents() == true){
				aiVector3D* tangents = &mesh->mMeshes[0]->mTangents[i];
				tan.push_back(glm::vec3(tangents->x, tangents->y, tangents->z)); // tangent buffer
		
				aiVector3D* bitangents = &mesh->mMeshes[0]->mBitangents[i];
				bitan.push_back(glm::vec3(bitangents->x, bitangents->y, bitangents->z)); // bitangent buffer
			}
		}

		for (unsigned int i = 0; i < mesh->mMeshes[0]->mNumFaces; i++) {
			const aiFace& Face = mesh->mMeshes[0]->mFaces[i];
			assert(Face.mNumIndices == 3);
			indices.push_back(Face.mIndices[0]);
			indices.push_back(Face.mIndices[1]);
			indices.push_back(Face.mIndices[2]);
		}

		// VBO for vertex data
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), v.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), n.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 2 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), t.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)2, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(2);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), tan.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)3, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(3);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumVertices*sizeof(GLfloat), bitan.data(), GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)4, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(4);

		glGenBuffers(1, &VBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * mesh->mMeshes[0]->mNumFaces*sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

		return VAO;
	}

	GLuint loadBitmap(char *fname) {
		GLuint texID;
		glGenTextures(1, &texID); // generate texture ID

		// load file - using core SDL library
		SDL_Surface *tmpSurface;
		tmpSurface = SDL_LoadBMP(fname);
		if (!tmpSurface) {
			std::cout << "Error loading bitmap" << std::endl;
		}

		// bind texture and set parameters
		glBindTexture(GL_TEXTURE_2D, texID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		SDL_PixelFormat *format = tmpSurface->format;

		GLuint externalFormat, internalFormat;
		if (format->Amask) {
			internalFormat = GL_RGBA;
			externalFormat = (format->Rmask < format->Bmask) ? GL_RGBA : GL_BGRA;
		}
		else {
			internalFormat = GL_RGB;
			externalFormat = (format->Rmask < format->Bmask) ? GL_RGB : GL_BGR;
		}

		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, tmpSurface->w, tmpSurface->h, 0,
			externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		glGenerateMipmap(GL_TEXTURE_2D);

		SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
		return texID;	// return value of texture ID
	}

	void drawMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive) {
		glBindVertexArray(mesh);	// Bind mesh VAO
		glDrawElements(primitive, indexCount, GL_UNSIGNED_INT, 0);	// draw VAO 
		glBindVertexArray(0);
	}

	void setLight(const GLuint program, const lightStruct light) {
		// pass in light data to shader
		int uniformIndex = glGetUniformLocation(program, "light.ambient");
		glUniform4fv(uniformIndex, 1, light.ambient);
		uniformIndex = glGetUniformLocation(program, "light.diffuse");
		glUniform4fv(uniformIndex, 1, light.diffuse);
		uniformIndex = glGetUniformLocation(program, "light.specular");
		glUniform4fv(uniformIndex, 1, light.specular);
	}


	void setMaterial(const GLuint program, const materialStruct material) {
		// pass in material data to shader 
		int uniformIndex = glGetUniformLocation(program, "material.ambient");
		glUniform4fv(uniformIndex, 1, material.ambient);
		uniformIndex = glGetUniformLocation(program, "material.diffuse");
		glUniform4fv(uniformIndex, 1, material.diffuse);
		uniformIndex = glGetUniformLocation(program, "material.specular");
		glUniform4fv(uniformIndex, 1, material.specular);
		uniformIndex = glGetUniformLocation(program, "material.shininess");
		glUniform1f(uniformIndex, material.shininess);
	}

	GLuint loadCubeMap(const char*fname[6], GLuint *texID)
	{
		glGenTextures(1, texID); // generate texture ID
		GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
			GL_TEXTURE_CUBE_MAP_POSITIVE_X,
			GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
			GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
			GL_TEXTURE_CUBE_MAP_NEGATIVE_Y };
		SDL_Surface *tmpSurface;
		glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); // bind texture and set parameters
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S,
			GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T,
			GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R,
			GL_CLAMP_TO_EDGE);
		for (int i = 0; i<6; i++){
			// load file - using core SDL library
			tmpSurface = SDL_LoadBMP(fname[i]);
			if (!tmpSurface){
				std::cout << "Error loading bitmap" << std::endl;
				return*texID;
			}
			glTexImage2D(sides[i], 0, GL_RGB, tmpSurface->w, tmpSurface->h, 0, GL_BGR, GL_UNSIGNED_BYTE, tmpSurface->pixels);
			// texture loaded, free the temporary buffer
			SDL_FreeSurface(tmpSurface);
		}
		return*texID;// return value of texure ID, redundant really
	}
}