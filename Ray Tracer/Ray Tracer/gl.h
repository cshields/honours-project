#pragma once

#define VERTEX		0
#define NORMAL		1
#define TEXCOORD    2
#define INDEX		3

#include <GL/glew.h>
#include <fstream>
#include <string>
#include <iostream>
#include <vector>
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags


namespace gl
{

	struct lightStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
	};

	struct materialStruct {
		GLfloat ambient[4];
		GLfloat diffuse[4];
		GLfloat specular[4];
		GLfloat shininess;
	};

	void exitFatalError(const char *message);
	GLuint initShader(const char *vertFile, const char *fragFile);
	GLuint createComputeShader(const char *compFile);
	char* loadFile(const char *fname, GLint &fSize);
	void setUniform(const GLuint program, int uniformIndex, const glm::mat4 data);
	void setUniform(const GLuint program, int uniformIndex, const glm::mat3 data);
	void setUniform(const GLuint program, int uniformIndex, const glm::vec4 data);
	void setUniform(const GLuint program, int uniformIndex, const glm::vec3 data);
	void setUniform(const GLuint program, int uniformIndex, const glm::vec2 data);
	void setUniform(const GLuint program, int uniformIndex, const GLfloat data);
	void setUniform(const GLuint program, int uniformIndex, const int data);
	GLuint importMesh(const aiScene* mesh);
	GLuint loadBitmap(char *fname);
	void drawMesh(const GLuint mesh, const GLuint indexCount, const GLuint primitive);
	void setLight(const GLuint program, const lightStruct light);
	void setMaterial(const GLuint program, const materialStruct material);
	GLuint loadCubeMap(const char*fname[6], GLuint *texID);

};

