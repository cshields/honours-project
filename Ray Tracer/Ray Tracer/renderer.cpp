#include "renderer.h"

renderer::renderer(GLuint mainShader, GLuint quadShader, GLuint width, GLuint height)
{
	shader = mainShader;
	quadProgram = quadShader;
	screenWidth = width;
	screenHeight = height;

	const aiScene* scene = importer.ReadFile("models/screen.obj", NULL);
	size[0] = scene->mMeshes[0]->mNumFaces * 3;
	mesh[0] = gl::importMesh(scene);

	scene = importer.ReadFile("models/cube.obj", aiProcess_Triangulate);

	// set point light values
	point.position = glm::vec4(5.0f, 0.0f, 0.0f, 1.0f);
	point.intensity = glm::vec4(1.0f);
	
	glUseProgram(shader);
	gl::setUniform(shader, LIGHT_INTENSITY, point.intensity);
	gl::setUniform(shader, NUMBER_OF_OBJECTS, numObjects);

	// create cube map
	const char *cubeTexFiles[6] = {
		"Textures/Town-skybox/Town_bk.bmp",
		"Textures/Town-skybox/Town_ft.bmp",
		"Textures/Town-skybox/Town_rt.bmp",
		"Textures/Town-skybox/Town_lf.bmp",
		"Textures/Town-skybox/Town_up.bmp",
		"Textures/Town-skybox/Town_dn.bmp"
	};
	loadCubeMap(cubeTexFiles, skybox);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, skybox[0]);

	populateBuffer(scene);

	vertices.shrink_to_fit();
	normals.shrink_to_fit();
	indices.shrink_to_fit();

	// translate models into their world positions
	for (int i = 0; i < vertices.size(); i++)
		vertices[i] = glm::translate(glm::mat4(1.0f), glm::vec3(-5.0f, -0.1f, -0.2f)) * vertices[i];

	// bind mesh data to buffer objects
	glGenBuffers(3, texBuf);
	glBindBuffer(GL_TEXTURE_BUFFER, texBuf[0]);
	glBufferData(GL_TEXTURE_BUFFER, sizeof(glm::vec4)*vertices.size(), vertices.data(), GL_DYNAMIC_DRAW);
	glBindBuffer(GL_TEXTURE_BUFFER, texBuf[1]);
	glBufferData(GL_TEXTURE_BUFFER, sizeof(glm::vec3)*normals.size(), normals.data(), GL_DYNAMIC_DRAW);
	glBindBuffer(GL_TEXTURE_BUFFER, texBuf[2]);
	glBufferData(GL_TEXTURE_BUFFER, sizeof(glm::ivec3)*indices.size(), indices.data(), GL_STATIC_DRAW);

	// bind buffer objects to texture buffers
	glGenTextures(3, imgTex);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_BUFFER, imgTex[0]);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, texBuf[0]);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_BUFFER, imgTex[1]);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F, texBuf[1]);
	glBindImageTexture(1, imgTex[0], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_BUFFER, imgTex[2]);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32I, texBuf[2]);
	glBindImageTexture(2, imgTex[1], 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);

	// place object properties into a vector
	objects.reserve(numObjects);
	// spheres
	objects.emplace_back(glm::vec4(0.0, 0.0, -5.0, 1.0), glm::vec3(0.0f), 5.0f, 0, 0);
	objects.emplace_back(glm::vec4(0.0, 0.0, 5.0, 1.0), glm::vec3(0.0f), 1.0f, 1, 0);
	objects.emplace_back(glm::vec4(0.0, 5.0, 0.0, 1.0), glm::vec3(0.0f), 1.0f, 2, 0);
	// planes
	objects.emplace_back(glm::vec4(0.0f), glm::vec3(0.0f, 1.0, 0.0), 20.0f, 3, 1);
	objects.emplace_back(glm::vec4(0.0f), glm::vec3(0.0f, -1.0, 0.0), 20.0f, 3, 1);
	objects.emplace_back(glm::vec4(0.0f), glm::vec3(0.0f, 0.0, 1.0), 20.0f, 0, 1);
	objects.emplace_back(glm::vec4(0.0f), glm::vec3(0.0f, 0.0, -1.0), 20.0f, 0, 1);
	objects.emplace_back(glm::vec4(0.0f), glm::vec3(1.0f, 0.0, 0.0), 20.0f, 1, 1);
	objects.emplace_back(glm::vec4(0.0f), glm::vec3(-1.0f, 0.0, 0.0), 20.0f, 1, 1);
	// mesh objects
	objects.emplace_back(glm::vec4(0.0, -0.5, -0.3, 1.0), glm::vec3(1.0f), 1.0f, 0, 3, 0, scene->mMeshes[0]->mNumFaces);

	//mat3x4 used to hold the material properties; ambient, diffuse and specular
	glm::mat3x4 materials[numObjects] = { glm::mat3x4{ glm::vec4(0.0, 0.0, 0.1, 0.5), glm::vec4(0.0, 0.0, 1.0, 0.1), glm::vec4(1.0, 1.0, 1.0, 100.0) },
									glm::mat3x4{ glm::vec4(0.1, 0.0, 0.0, 0.0), glm::vec4(1.0, 0.0, 0.0, 0.5), glm::vec4(1.0, 1.0, 1.0, 100.0) }, 
									glm::mat3x4{ glm::vec4(0.0, 0.1, 0.0, 0.0), glm::vec4(0.0, 1.0, 0.0, 0.0), glm::vec4(1.0, 1.0, 1.0, 200.0) }, 
									glm::mat3x4{ glm::vec4(0.1, 0.0, 0.1, 0.0), glm::vec4(0.5, 0.0, 0.5, 0.0), glm::vec4(1.0, 1.0, 1.0, 10.0) } };

	// bind object and material properties to storage buffers
	glGenBuffers(2, sphereBufs);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, sphereBufs[1]);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(materials), materials, GL_STATIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, sphereBufs[0]);
	glBufferData(GL_SHADER_STORAGE_BUFFER, numObjects * sizeof(Object), objects.data(), GL_DYNAMIC_DRAW);
}

renderer::~renderer()
{
}


void renderer::sceneDraw(glm::mat4 cam, glm::vec3 eye){
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	std::vector<Object> temp;
	temp = objects; // copy objects into a temporary vector for transformations
	for (int i = 0; i < numObjects; i++){
		temp[i].position = cam * temp[i].position;		//transforms the objects using the modelview matrix
		if (objects[i].type != 3)						// dont want to transform the normal variable for meshes because its used to store the bounding volume parameters
			temp[i].normal = glm::mat3(cam) * objects[i].normal;
	}	

	// sets distance from the camera to the planes
	temp[3].radius = temp[3].radius + eye.y;
	temp[4].radius = temp[4].radius - eye.y;
	temp[5].radius = temp[5].radius + eye.z;
	temp[6].radius = temp[6].radius - eye.z;
	temp[7].radius = temp[7].radius + eye.x;
	temp[8].radius = temp[8].radius - eye.x;

	std::vector<glm::vec3> tempNorm = normals;
	std::vector<glm::vec4> tempVerts = vertices;
	//transforms the mesh data using the modelview matrix
	for (int i = 0; i < vertices.size(); i++) {
		tempNorm[i] = glm::mat3(cam) * normals[i];
		tempVerts[i] = cam * vertices[i];
	}

	// copy transformed data into the buffers
	glBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, numObjects*sizeof(Object), temp.data());
	glBindBuffer(GL_TEXTURE_BUFFER, texBuf[0]);
	glBufferSubData(GL_TEXTURE_BUFFER, 0, sizeof(glm::vec4)*tempVerts.size(), tempVerts.data());
	glBindBuffer(GL_TEXTURE_BUFFER, texBuf[1]);
	glBufferSubData(GL_TEXTURE_BUFFER, 0, sizeof(glm::vec3)*tempNorm.size(), tempNorm.data());

	glm::vec4 light = cam * point.position;

	glUseProgram(shader);
	gl::setUniform(shader, LIGHT_POSITION, light);
	gl::setUniform(shader, ROTATION_MATRIX, glm::mat3(glm::transpose(cam)));
	
	glDispatchCompute(screenWidth / 16, screenHeight / 9, 1); // dispatch the compute shader to run the ray tracer
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);	//synchronise rendering to the texture before rendering to the screen

	glUseProgram(quadProgram);
	gl::drawMesh(mesh[0], size[0], GL_TRIANGLES); // draw the screen quad
}

void renderer::populateBuffer(const aiScene* scene){
	// copy mesh data into vectors
	for (int i = 0; i < scene->mMeshes[0]->mNumVertices; i++){
		aiVector3D* verts = &scene->mMeshes[0]->mVertices[i];
		vertices.push_back(glm::vec4(verts->x, verts->y, verts->z, 1.0f)); // vertex buffer

		if (scene->mMeshes[0]->HasNormals() == true){
			aiVector3D* norms = &scene->mMeshes[0]->mNormals[i];
			normals.push_back(glm::vec3(norms->x, norms->y, norms->z)); // normal buffer
		}
	}
	// copy mesh indices into vector
	for (unsigned int i = 0; i < scene->mMeshes[0]->mNumFaces; i++) {
		const aiFace& Face = scene->mMeshes[0]->mFaces[i];
		assert(Face.mNumIndices == 3);
		indices.push_back(glm::ivec3(Face.mIndices[0], Face.mIndices[1], Face.mIndices[2]));
	}


}

void renderer::loadCubeMap(const char*fname[6], GLuint *texID)
{
	glGenTextures(1, texID); // generate texture ID
	GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y };
	SDL_Surface *tmpSurface;
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); // bind texture and set parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S,
		GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T,
		GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R,
		GL_CLAMP_TO_EDGE);
	for (int i = 0; i<6; i++){
		// load file - using core SDL library
		tmpSurface = SDL_LoadBMP(fname[i]);
		if (!tmpSurface){
			std::cout << "Error loading bitmap" << std::endl;
		}
		glTexImage2D(sides[i], 0, GL_RGB, tmpSurface->w, tmpSurface->h, 0, GL_BGR, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		// texture loaded, free the temporary buffer
		SDL_FreeSurface(tmpSurface);
	}
}
