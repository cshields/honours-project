#version 440

in vec2 outTexCoords;

layout(location = 0) out vec4 frag_Colour;

layout (binding = 0) uniform sampler2D colourImg;

void main(void)
{
	frag_Colour = texture(colourImg, outTexCoords);
}