#include "camera.h"


camera::camera(GLuint shader, GLfloat vert, glm::vec2 screenRes)
{
	eye = glm::vec3(0.0f);
	at = glm::vec3(0.0f, 0.0f, -1.0f);
	up = glm::vec3(0.0f, 1.0f, 0.0f);
	rX = 0.0f;
	rY = 0.0f;

	// create the field of view(Hor+ used)
	GLfloat height = glm::tan(glm::radians(vert * 0.5f));
	GLfloat width = height * (16.0f / 9.0f);
	glm::vec2 fov = glm::vec2(width, height);

	enum uniforms {FIELD_OF_VIEW, PIXEL_WIDTH};
	gl::setUniform(shader, FIELD_OF_VIEW, -fov);
	gl::setUniform(shader, PIXEL_WIDTH, glm::vec2(2 * fov.x / screenRes.x, 2 * fov.y / screenRes.y)); // the width between each pixel is pre calculated to save processing it in the compute shader
}


camera::~camera()
{
}

void camera::moveForward(GLfloat d) {
	eye += d*facing;
}

void camera::moveRight(GLfloat d) {
	 eye += (d * glm::cross(up, facing));
}

void camera::moveY(GLfloat d){
	eye.y += d;
}

void camera::rotateCamera(){
	int x, y;
	SDL_GetRelativeMouseState(&x, &y);
	rX -= x*0.2f;
	rY -= y*0.2f;

	at = glm::vec3(0.0f, 0.0f, -1.0f); // at is the direction the camera is pointing towards
	facing = glm::vec3(0.0f, 0.0f, -1.0f); // facing is the direction the camera faces on the x and z axes only
	glm::quat Y = glm::angleAxis(rX, glm::vec3(0.0f, 1.0f, 0.0f));
	glm::quat X = glm::angleAxis(rY, glm::vec3(1.0f, 0.0f, 0.0f));
	glm::quat temp = Y * X;
	facing = Y * facing; // facing used to keep the camera movements parallel to the x and z axes
	eyeDir = temp * at;
	at = eyeDir + eye;
}