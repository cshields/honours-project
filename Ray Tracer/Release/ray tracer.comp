#version 440

layout (local_size_x = 16, local_size_y = 9) in;

struct light{
	vec4 position;
	vec4 intensity;
};

struct Object{
	vec4 position;
	vec3 normal;
	float radius; // used for distance with planes
	int matIdx;
	int type;
	int bufferIdx;
	int numFaces;
};

struct objNode{ // stores information of the object hit at each level in the ray tree 
	int objectIndex;
	vec3 rayDirection, rayOrigin, normal;
	bool reflectionVisited, refractionVisited;
};

restrict layout(std430, binding=0) buffer objects{
	Object object[];
};
restrict layout(std430, binding=1) buffer colours{
	mat3x4 material[];
};

restrict layout (binding = 0, rgba8) uniform image2D ColourImg;
layout (binding = 1) uniform samplerBuffer verts;
layout (binding = 2) uniform samplerBuffer normals;
layout (binding = 3) uniform isamplerBuffer indices;
uniform samplerCube cubeMap;

layout (location = 0) uniform vec2 fov;
layout (location = 1) uniform vec2 increment;
layout (location = 2) uniform light pointLight;
layout (location = 4) uniform mat3 rotation;
layout (location = 5) uniform int numObjects;

const int MAX_DEPTH = 3;

shared objNode rayTree[16*9][MAX_DEPTH];

vec4 phong(Object hitObject, vec3 intersection, vec3 rayDir){
	vec3 lightDirection = normalize(pointLight.position.xyz - intersection);
	vec3 halfwayVector = normalize(lightDirection + -rayDir);

	vec4 ambient = material[hitObject.matIdx][0];

	vec4 diffuse = pointLight.intensity * material[hitObject.matIdx][1];
	float coefficient = max(dot(hitObject.normal, lightDirection), 0.0f);
	diffuse = diffuse * coefficient;

	vec4 specular = vec4(0.0f);
	if (coefficient > 0.0f){
		specular = pointLight.intensity * material[hitObject.matIdx][2];
		specular = specular * pow(max(dot(halfwayVector, hitObject.normal), 0.0f), material[hitObject.matIdx][2].w);
	}

	return ambient + diffuse + specular;
}

bool sphereIntersection(Object obj, vec3 ray, vec3 origin, out float t){
	vec3 EO = obj.position.xyz - origin;
	float v = dot(EO, ray);

	// if v < 0 then the object is behind the ray origin. if d < 0 the ray doesn't intersect
	if (v > 0){	
		float d = obj.radius * obj.radius - (dot(EO, EO) - v * v);
		if (d > 0){
			t = v - sqrt(d);
			return true;
		}
	}
	return false;
}

bool planeIntersection(Object obj, vec3 ray, vec3 origin, out float t){
	float v = dot(ray, obj.normal);
	// if v > 0 then the camera is facing the back face
	if (v < 0){
		vec3 position = (obj.normal * obj.radius) + origin; // finds the closest point on the plane relative to the ray's origin
		float d = dot(obj.normal, origin) + distance(origin, position);
		// if < 0 then the plane is behind the ray
		if (dot(position, obj.normal) > 0){
			t = -(d) / v;
			return true;
		}
	}
	return false;
}

bool boxIntersection(Object obj, vec3 ray, vec3 origin) {
	vec3 delta = obj.position.xyz - origin;

	vec3 axis;
	float e, f, t1, t2,	tmax = 10000000.0f, tmin = -10000000.0f;;

	for (int i = 0; i < 3; i++){
		axis = vec3(rotation[0][i], rotation[1][i], rotation[2][i]);
		e = dot(axis, delta);
		f = 1.0f/dot(ray, axis);

		t1 = (e + obj.normal[i]) *f;
		t2 = (e - obj.normal[i]) *f;
 
		tmin = max(tmin, min(t1, t2));
		tmax = min(tmax, max(t1, t2));
	}	

	return tmax >= tmin && tmin > 0;
}

bool triangleIntersection(vec3 ray, vec3 origin, int bufIdx, out ivec3 faceIndices, out vec2 uv, out float t)
{
	faceIndices = texelFetch(indices, bufIdx).xyz;
    vec3 edge1 = texelFetch(verts, faceIndices.y).xyz - texelFetch(verts, faceIndices.x).xyz;
    vec3 edge2 = texelFetch(verts, faceIndices.z).xyz - texelFetch(verts, faceIndices.x).xyz;
    vec3 pvec = cross(ray, edge2);
    float det = dot(edge1, pvec);

    if (det <= 0) 
		return false;

	float invDet = 1 / det;
    vec3 tvec = origin - texelFetch(verts, faceIndices.x).xyz;
    uv.x = dot(tvec, pvec) * invDet;

    if (uv.x < 0 || uv.x > 1) // if true ray misses
		return false;

    vec3 qvec = cross(tvec, edge1);
	uv.y = dot(ray, qvec) * invDet;

    if (uv.y < 0 || uv.x + uv.y > 1) // if true ray misses
		return false;

    t = dot(edge2, qvec) * invDet;

    return t > 0; // if t < 0 the intersection is behind the ray
}

bool meshIntersection(inout Object obj, vec3 ray, vec3 origin, out float t){
	//if(!boxIntersection(obj, ray, origin))
		//return false;
	
	float current = t = 10000000.0f, w;
	ivec3 faceIndices;
	vec2 uv;
	for(int i = 0; i < obj.numFaces; i++){
		if (triangleIntersection(ray, origin, i+obj.bufferIdx, faceIndices, uv, current)){
			if(t > current){
				t = current;
				w = 1 - uv.x - uv.y;
				obj.normal = w*texelFetch(normals, faceIndices.x).xyz + // interpolates the normal data using the barycentric coordinates
							uv.x*texelFetch(normals, faceIndices.y).xyz +
							uv.y*texelFetch(normals, faceIndices.z).xyz;
			}
		}
	}

	return t < 10000000.0f;
}

bool objectIntersection(inout Object obj, vec3 ray, vec3 origin, out float t){
	switch(obj.type){
	case 0:
		return sphereIntersection(obj, ray, origin, t);
	case 1:
		return planeIntersection(obj, ray, origin, t);
	case 2:
		return boxIntersection(obj, ray, origin);
	case 3:
		return meshIntersection(obj, ray, origin, t);
	}
	return false;
}

bool shadowRay(vec3 origin, float skip){
	Object obj;
	float distToObj, distToLight;
	vec3 dirToLight = pointLight.position.xyz - origin;
	vec3 rayDir = normalize(dirToLight);
	distToLight = length(dirToLight);

	for(int i = 0; i < numObjects; i++){
		if (i != skip){
			obj = object[i];
			if(objectIntersection(obj, rayDir, origin, distToObj))
				if(distToObj < distToLight)
					return true;
		}
	}
	return false;
}

vec4 rayShade(vec3 rayDir, inout vec3 origin, out bool hit, inout int objectIdx, out vec3 normal){
	float new, old;
	int skip = objectIdx;
	Object currentObj;

	hit = false;
	for(int i = 0; i < numObjects; i++){
		currentObj = object[i];
		if(i != skip)													
			if(hit){															
				if(objectIntersection(currentObj, rayDir, origin, new))
					if(old > new){		// ignores the current object if it is further away than the closest object so far
						old = new;
						objectIdx = i;
						hit = true;
						normal = currentObj.normal;
					}
			}
			else{
				hit = objectIntersection(currentObj, rayDir, origin, old);
				normal = currentObj.normal;
				objectIdx = i;
			}
	}

	if(hit){	
		// sets up the variables for the next reflected ray
		origin = old*rayDir + origin;
		Object hitObject = object[objectIdx];

		if (hitObject.type == 0)
			normal = hitObject.normal = normalize(origin - hitObject.position.xyz);
		//return vec4(normal, 1.0f);
		hitObject.normal = normal;

		if (!shadowRay(origin, objectIdx))
			return phong(hitObject, origin, rayDir);
		else
			return material[hitObject.matIdx][0];
	}

	return texture(cubeMap, rotation * rayDir);
}

void main(){

	bool objectHit;
	vec4 finalColour;

	for(int i = 0; i < MAX_DEPTH; i++){
		rayTree[gl_LocalInvocationIndex][i].objectIndex = -1;
		rayTree[gl_LocalInvocationIndex][i].reflectionVisited = rayTree[gl_LocalInvocationIndex][i].refractionVisited = false;
	}

	float l, d;

	rayTree[gl_LocalInvocationIndex][0].rayDirection = normalize(vec3(gl_GlobalInvocationID.x * increment.x + fov.x, gl_GlobalInvocationID.y * increment.y + fov.y, -1.0f));
	rayTree[gl_LocalInvocationIndex][0].rayOrigin = vec3(0.0f);
	finalColour = rayShade(rayTree[gl_LocalInvocationIndex][0].rayDirection, rayTree[gl_LocalInvocationIndex][0].rayOrigin, objectHit, rayTree[gl_LocalInvocationIndex][0].objectIndex, rayTree[gl_LocalInvocationIndex][0].normal);

	int depth = 1; 
	while(depth > 0 ){
		if(objectHit && depth < MAX_DEPTH){
			if(material[object[rayTree[gl_LocalInvocationIndex][depth-1].objectIndex].matIdx][1].w != 0.0f && !rayTree[gl_LocalInvocationIndex][depth].reflectionVisited && objectHit){ // checks if the object is reflective and if it its reflective path has been visited yet
				rayTree[gl_LocalInvocationIndex][depth].rayDirection = reflect(rayTree[gl_LocalInvocationIndex][depth-1].rayDirection, rayTree[gl_LocalInvocationIndex][depth-1].normal);	// sets the reflected direction
				rayTree[gl_LocalInvocationIndex][depth].objectIndex = rayTree[gl_LocalInvocationIndex][depth-1].objectIndex;																// sets the object index for the current depth using the previous object in the tree
				rayTree[gl_LocalInvocationIndex][depth].rayOrigin = rayTree[gl_LocalInvocationIndex][depth-1].rayOrigin;																	// sets the ray origin for the current depth using the previous object in the tree
				finalColour = mix(finalColour, rayShade(rayTree[gl_LocalInvocationIndex][depth].rayDirection, rayTree[gl_LocalInvocationIndex][depth].rayOrigin, objectHit, rayTree[gl_LocalInvocationIndex][depth].objectIndex, rayTree[gl_LocalInvocationIndex][depth].normal), material[object[rayTree[gl_LocalInvocationIndex][depth-1].objectIndex].matIdx][1].w);
				rayTree[gl_LocalInvocationIndex][depth].reflectionVisited = true;
				depth++;
			}
			else{
				if(material[object[rayTree[gl_LocalInvocationIndex][depth-1].objectIndex].matIdx][0].w != 0.0f && !rayTree[gl_LocalInvocationIndex][depth].refractionVisited && objectHit){	// checks if the object is refractive and if it its refractive path has been visited yet
					rayTree[gl_LocalInvocationIndex][depth].rayDirection = refract(rayTree[gl_LocalInvocationIndex][depth-1].rayDirection, rayTree[gl_LocalInvocationIndex][depth-1].normal, 1.0f/1.52f); // sets the refractive direction
					rayTree[gl_LocalInvocationIndex][depth].objectIndex = rayTree[gl_LocalInvocationIndex][depth-1].objectIndex;																			// sets the object index for the current depth using the previous object in the tree
					rayTree[gl_LocalInvocationIndex][depth].rayOrigin = rayTree[gl_LocalInvocationIndex][depth-1].rayOrigin;																				// sets the ray origin for the current depth using the previous object in the tree
					finalColour = mix(finalColour, rayShade(rayTree[gl_LocalInvocationIndex][depth].rayDirection, rayTree[gl_LocalInvocationIndex][depth].rayOrigin, objectHit, rayTree[gl_LocalInvocationIndex][depth].objectIndex, rayTree[gl_LocalInvocationIndex][depth].normal), material[object[rayTree[gl_LocalInvocationIndex][depth-1].objectIndex].matIdx][0].w);
					rayTree[gl_LocalInvocationIndex][depth].refractionVisited = true;
					depth++;
				}
				else{
					// once all possible paths for an object have been exhausted we  reset the booleans at this layer before steping back a layer
					rayTree[gl_LocalInvocationIndex][depth].refractionVisited = false;
					rayTree[gl_LocalInvocationIndex][depth].reflectionVisited = false;
					depth--;
					objectHit = true;
				}
			}
		}
		else{
			// once the max depth is reached or a ray returns a miss we  reset the booleans at this layer before steping back a layer
			rayTree[gl_LocalInvocationIndex][depth].refractionVisited = false;
			rayTree[gl_LocalInvocationIndex][depth].reflectionVisited = false;
			depth--;
			objectHit = true;
		}
	}
	imageStore(ColourImg, 
				ivec2(gl_GlobalInvocationID.xy), finalColour);
}