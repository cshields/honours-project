# README #

This is the implementation for my honours project. The aim is to produce a real time ray tracer.

To run the code glm, glew, sdl and assimp are required.
An .exe file has been included in the release folder.

# Controls #
WASD + mouse for camera controls + R&F to ascend and descend respectively
arrow keys move one of the point lights